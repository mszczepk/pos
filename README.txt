The project is made with the following assumptions:
1. IO device interfaces return and accept objects of the String class, for simplicity.
2. The main function is in the Application class. The scanner object is responsible for
   reading all input data (not only those from barcodes).
3. The ProductDatabase class is a stub. An instance of this class contains seven products
   with barcodes '111', '222','333', etc.