package mszczepk.pos;

import mszczepk.pos.io.StreamDisplay;
import mszczepk.pos.io.StreamPrinter;
import mszczepk.pos.io.StreamScanner;
import mszczepk.pos.persistance.ProductDatabase;

/**
 * @author Mateusz Szczepkowski
 */
public class Application {
    public static void main(String[] args) {
        StreamScanner scanner = new StreamScanner(System.in);
        StreamDisplay display = new StreamDisplay(System.out);
        StreamPrinter printer = new StreamPrinter(System.out);
        ProductDatabase database = new ProductDatabase();
        Format receiptFormat = new Format(" COMPANY SP.Z.O.O. ", 40, 0.75f, ' ', '.', "EUR");
        SalePoint salePoint = new SalePoint(scanner, database, display, printer, receiptFormat);

        System.out.println("Type barcode ('111', '222', etc.) and press ENTER to scan a product.");
        System.out.println("Type 'exit' and press ENTER to sum up.");
        System.out.println("Type 'quit' and press ENTER to quit application.");
        System.out.println("");

        display.info("Welcome!");
        while (true) {
            switch (scanner.readInput()) {
                case "exit":
                    salePoint.exit();
                    break;
                case "quit":
                    display.info("Thank you! Come again!");
                    System.exit(0);
                    break;
                default:
                    salePoint.sale();
                    break;
            }
        }
    }
}
