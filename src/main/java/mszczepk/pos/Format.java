package mszczepk.pos;

/**
 * The <code>Format</code> class stores information about how a receipt should be printed in a printer device.
 * @author Mateusz Szczepkowski
 */
public class Format {
    private final String companyName;
    private final int width;
    private final float nameMaxWidth;
    private final char currencyGroupingSeparator;
    private final char currencyDecimalSeparator;
    private final String currencyCode;

    public Format(String companyName, int width, float nameMaxWidth, char currencyGroupingSeparator, char currencyDecimalSeparator, String currencyCode) {
        if (companyName == null) {
            throw new IllegalArgumentException("Company name can't be null!");
        } else if (nameMaxWidth < 0f || nameMaxWidth > 1f) {
            throw new IllegalArgumentException("Name max with must be a number between 0 and 1 (exclusivly)!");
        } else if (currencyCode == null) {
            throw new IllegalArgumentException("Currency code can't be null!");
        }
        this.companyName = companyName;
        this.nameMaxWidth = nameMaxWidth;
        this.width = width;
        this.currencyGroupingSeparator = currencyGroupingSeparator;
        this.currencyDecimalSeparator = currencyDecimalSeparator;
        this.currencyCode = currencyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public int getWidth() {
        return width;
    }

    public float getNameMaxWidth() {
        return nameMaxWidth;
    }

    public char getCurrencyGroupingSeparator() {
        return currencyGroupingSeparator;
    }

    public char getCurrencyDecimalSeparator() {
        return currencyDecimalSeparator;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }
}
