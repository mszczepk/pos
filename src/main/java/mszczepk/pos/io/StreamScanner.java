package mszczepk.pos.io;

import java.io.InputStream;

/**
 * @author Mateusz Szczepkowski
 */
public class StreamScanner implements Scanner {
    private final java.util.Scanner in;
    private String input;

    public StreamScanner(InputStream in) {
        if (in == null) {
            throw new IllegalArgumentException("Input can't be null!");
        }
        this.in = new java.util.Scanner(in);
        input = "";
    }

    public String readInput() {
        input = in.nextLine();
        return input;
    }

    @Override
    public String getBarcode() {
        return input;
    }
}
