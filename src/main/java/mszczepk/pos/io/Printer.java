package mszczepk.pos.io;

/**
 * @author Mateusz Szczepkowski
 */
public interface Printer {
    /**
     * Prints a printable object using a printer device represented by this <code>Printer</code> object.
     * @param printable A string representation of the printable object.
     */
    void print(String printable);
}
