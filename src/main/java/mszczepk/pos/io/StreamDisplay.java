package mszczepk.pos.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Mateusz Szczepkowski
 */
public class StreamDisplay implements Display {
    private final OutputStream out;

    public StreamDisplay(OutputStream out) {
        if (out == null) {
            throw new IllegalArgumentException("Output can't be null!");
        }
        this.out = out;
    }

    @Override
    public void info(String message) {
        if (message == null) {
            message = "null";
        }
        try {
            out.write('[');
            out.write(message.getBytes());
            out.write(']');
            out.write('\n');
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void error(String message) {
        info(message);
    }
}
