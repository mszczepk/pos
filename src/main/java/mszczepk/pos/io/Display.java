package mszczepk.pos.io;

/**
 * @author Mateusz Szczepkowski
 */
public interface Display {
    /**
     * Displays an info message on an LCD device represented by this <code>Display</code> object.
     */
    void info(String message);

    /**
     * Displays an error message on an LCD device represented by this <code>Display</code> object.
     */
    void error(String message);
}
