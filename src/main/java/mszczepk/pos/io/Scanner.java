package mszczepk.pos.io;

/**
 * @author Mateusz Szczepkowski
 */
public interface Scanner {
    /**
     * Reads the current input from a barcode scanner device represented by this <code>Scanner</code> object.
     * @return a non-empty string representation of a scanned barcode or an empty string (<code>""</code>) if no barcode
     * was scanned.
     */
    String getBarcode();
}
