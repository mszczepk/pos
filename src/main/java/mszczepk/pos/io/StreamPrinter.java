package mszczepk.pos.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Mateusz Szczepkowski
 */
public class StreamPrinter implements Printer {
    private final OutputStream out;

    public StreamPrinter(OutputStream out) {
        if (out == null) {
            throw new IllegalArgumentException("Output can't be null!");
        }
        this.out = out;
    }

    @Override
    public void print(String printable) {
        if (printable == null) {
            printable = "null";
        }
        try {
            out.write(printable.getBytes());
            out.write('\n');
            out.write('\n');
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
