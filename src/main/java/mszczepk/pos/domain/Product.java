package mszczepk.pos.domain;

import java.math.BigDecimal;

/**
 * @author Mateusz Szczepkowski
 */
public class Product {
    private String barcode;
    private String name;
    private BigDecimal priceInSubcurrency;

    public Product(String barcode, String name, BigDecimal priceInSubcurrency) {
        if (barcode == null) {
            throw new IllegalArgumentException("Barcode cannot be null!");
        } else if (barcode.isEmpty()) {
            throw new IllegalArgumentException("Barcode cannot be an empty string!");
        } else if (name == null) {
            throw new IllegalArgumentException("Name cannot be null!");
        } else if (name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be an empty string!");
        } else if (priceInSubcurrency == null) {
            throw new IllegalArgumentException("Price cannot be null!");
        } else if (priceInSubcurrency.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Price cannot be negative!");
        }
        this.barcode = barcode;
        this.name = name;
        this.priceInSubcurrency = priceInSubcurrency;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        if (barcode == null) {
            throw new IllegalArgumentException("Barcode cannot be null!");
        } else if (barcode.isEmpty()) {
            throw new IllegalArgumentException("Barcode cannot be an empty string!");
        }
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null!");
        } else if (name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be an empty string!");
        }
        this.name = name;
    }

    public BigDecimal getPriceInSubcurrency() {
        return priceInSubcurrency;
    }

    public void setPriceInSubcurrency(BigDecimal priceInSubcurrency) {
        if (priceInSubcurrency == null) {
            throw new IllegalArgumentException("Price cannot be null!");
        } else if (priceInSubcurrency.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Price cannot be negative!");
        }
        this.priceInSubcurrency = priceInSubcurrency;
    }
}
