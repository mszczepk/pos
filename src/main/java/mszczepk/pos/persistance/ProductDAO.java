package mszczepk.pos.persistance;

import mszczepk.pos.domain.Product;
import java.util.Optional;

/**
 * @author Mateusz Szczepkowski
 */
public interface ProductDAO {
    Optional<Product> getProductByBarcode(String barcode);
}
