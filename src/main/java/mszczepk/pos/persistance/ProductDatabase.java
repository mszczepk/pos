package mszczepk.pos.persistance;

import mszczepk.pos.domain.Product;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Mateusz Szczepkowski
 */
public class ProductDatabase implements ProductDAO {
    private final Map<String, Product> products;

    public ProductDatabase() {
        products = new HashMap<>(5);
        addProduct("7up Napój gazowany 2L", "289");
        addProduct("Deser Monte MAX 4x100g", "539");
        addProduct("Kiełbasa podwawelska z szynki", "1199");
        addProduct("Olej kujawski", "679");
        addProduct("Płyn do mycia szyb AJAX", "549");
        addProduct("Żarówka LED Phillips", "899");
        addProduct("Skarpetki damskie PUMA", "1999");
    }

    private void addProduct(String name, String priceInSubcurrency) {
        String barcode = "xxx".replace("x", String.valueOf(products.size() + 1));
        products.put(barcode, new Product(barcode, name, new BigDecimal(priceInSubcurrency)));
    }

    @Override
    public Optional<Product> getProductByBarcode(String barcode) {
        return Optional.ofNullable(products.get(barcode));
    }
}
