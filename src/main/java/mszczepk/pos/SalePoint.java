package mszczepk.pos;

import mszczepk.pos.domain.Product;
import mszczepk.pos.persistance.ProductDAO;
import mszczepk.pos.io.Display;
import mszczepk.pos.io.Printer;
import mszczepk.pos.io.Scanner;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * @author Mateusz Szczepkowski
 */
public class SalePoint {
    private Scanner scanner;
    private ProductDAO productDAO;
    private Display display;
    private Printer printer;

    private Formatter receipt; //used to prettily-format receipt string
    private BigDecimal totalPriceInSubcurrency;

    public SalePoint(Scanner scanner, ProductDAO productDAO, Display display, Printer printer, Format format) {
        if (scanner == null) {
            throw new IllegalArgumentException("Scanner can't be null!");
        } else if (productDAO == null) {
            throw new IllegalArgumentException("Product DAO can't be null!");
        } else if (display == null) {
            throw new IllegalArgumentException("Display can't be null!");
        } else if (printer == null) {
            throw new IllegalArgumentException("Printer can't be null!");
        } else if (format == null) {
            throw new IllegalArgumentException("Format can't be null!");
        }

        this.scanner = scanner;
        this.productDAO = productDAO;
        this.display = display;
        this.printer = printer;

        this.receipt = new Formatter(format);
        totalPriceInSubcurrency = BigDecimal.ZERO;
    }

    public void sale() {
        String barcode = scanner.getBarcode();
        if (barcode.isEmpty()) {
            display.error("Invalid bar-code");
            return;
        }
        Optional<Product> product = productDAO.getProductByBarcode(barcode);
        if (product.isPresent()) {
            String name = product.get().getName();
            BigDecimal priceInSubcurrency = product.get().getPriceInSubcurrency();
            String priceInCurrency = receipt.formatPrice(priceInSubcurrency);

            totalPriceInSubcurrency = totalPriceInSubcurrency.add(priceInSubcurrency);
            receipt.append(name, priceInCurrency);
            display.info(name + " " + priceInCurrency);
        } else {
            display.error("Product not found");
        }
    }

    public void exit() {
        String totalPriceInCurrency = receipt.formatPrice(totalPriceInSubcurrency);
        receipt.append("Total", totalPriceInCurrency);
        printer.print(receipt.build());
        display.info("Total " + totalPriceInCurrency);
        totalPriceInSubcurrency = BigDecimal.ZERO; //reset the total price
    }

    private class Formatter {
        private final String companyName;
        private final int width;
        private final List<Entry> receipt;
        private final String formatPattern;
        private final Currency currency;
        private final DecimalFormat currencyFormat;

        private Formatter(Format format) {
            this.companyName = format.getCompanyName();
            this.width = format.getWidth();
            receipt = new ArrayList<>();
            int nameWidth = (int) (format.getNameMaxWidth() * width);
            int priceWidth = width - nameWidth;
            formatPattern = String.format("%%-%1$d.%1$ds%%%2$ds", nameWidth, priceWidth); //e.g. if format arguments are 30 and 10 then pattern is "%-30.30s%10s"
            currency = Currency.getInstance(format.getCurrencyCode());
            DecimalFormatSymbols currencyFormatSymbols = new DecimalFormatSymbols();
            currencyFormatSymbols.setGroupingSeparator(format.getCurrencyGroupingSeparator());
            currencyFormatSymbols.setDecimalSeparator(format.getCurrencyDecimalSeparator());
            currencyFormat = new DecimalFormat();
            currencyFormat.setDecimalFormatSymbols(currencyFormatSymbols);
            currencyFormat.setMinimumFractionDigits(currency.getDefaultFractionDigits());
        }

        private void append(String name, String priceInCurrency) {
            receipt.add(new Entry(name, priceInCurrency));
        }

        private String build() {
            StringBuilder builder = new StringBuilder();
            appendLineCentered(builder, companyName, '=');
            Iterator<Entry> it = receipt.iterator();
            while (it.hasNext()) {
                Entry entry = it.next();
                if (!it.hasNext()) {
                    //process the last element
                    appendLineFilled(builder, '-');
                    appendLineEntry(builder, entry.name + " " + currency.getCurrencyCode(), entry.price);
                    it.remove();
                    break;
                }
                appendLineEntry(builder, entry.name, entry.price);
                it.remove();
            }
            builder.deleteCharAt(builder.length() - 1); //remove the last '\n' char from the string builder
            return builder.toString();
        }

        private String formatPrice(BigDecimal priceInSubcurrency) {
            return currencyFormat.format(subcurrencyToCurrency(priceInSubcurrency));
        }

        /* utility methods for Formatter class */

        private void appendLineCentered(StringBuilder builder, String string, char filler) {
            int start = (width - string.length()) / 2;
            for (int i = 0; i <= width; i++) {
                if (i == start) {
                    builder.append(string);
                    i += string.length();
                } else {
                    builder.append(filler);
                }
            }
            builder.append('\n');
        }

        private void appendLineEntry(StringBuilder builder, String name, String price) {
            builder.append(String.format(formatPattern, name, price));
            builder.append('\n');
        }

        private void appendLineFilled(StringBuilder builder, char filler) {
            for (int i = 0; i < width; i++) {
                builder.append(filler);
            }
            builder.append('\n');
        }

        private BigDecimal subcurrencyToCurrency(BigDecimal priceInSubcurrency) {
            return priceInSubcurrency.movePointLeft(currency.getDefaultFractionDigits());
        }

        private class Entry {
            private final String name;
            private final String price;

            private Entry(String name, String price) {
                this.name = name;
                this.price = price;
            }
        }
    }
}
