package mszczepk.pos;

import mszczepk.pos.domain.Product;
import mszczepk.pos.persistance.ProductDAO;
import mszczepk.pos.io.Display;
import mszczepk.pos.io.Printer;
import mszczepk.pos.io.Scanner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import java.math.BigDecimal;
import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author Mateusz Szczepkowski
 */
@RunWith(MockitoJUnitRunner.class)
public class SalePointTest {
    @Mock
    private Scanner barcodeScanner;
    @Mock
    private ProductDAO productDAO;
    @Mock
    private Display display;
    @Mock
    private Printer printer;
    @Spy
    private Format receiptFormat = new Format(" COMPANY SP.Z.O.O ", 40, 0.75f, ' ', '.', "EUR");
    @InjectMocks
    private SalePoint salePoint;

    @Test
    public void barcodeFoundThenNameAndPriceIsDisplayed() {
        provideProduct("1111111111", "Łaciate Milk 1L", "135");
        barcodeScanned("1111111111");
        salePoint.sale();
        verify(display, times(1)).info(ArgumentMatchers.eq("Łaciate Milk 1L 1.35"));
        verifyNoMoreInteractions(display, printer);
    }

    @Test
    public void barcodeNotFoundThenErrorMessageIsDisplayed() {
        barcodeScanned("2222222222");
        salePoint.sale();
        verify(display, times(1)).error(ArgumentMatchers.eq("Product not found"));
        verifyNoMoreInteractions(display, printer);
    }

    @Test
    public void barcodeEmptyThenErrorMessageIsDisplayed() {
        barcodeScanned("");
        salePoint.sale();
        verify(display, times(1)).error(ArgumentMatchers.eq("Invalid bar-code"));
        verifyNoMoreInteractions(display, printer);
    }

    @Test
    public void whenNoProductsScannedAndExitThenReceiptIsPrintedAndTotalIsDisplayed() {
        salePoint.exit();
        String expectedMessage = "Total 0.00";
        String expectedReceipt = "=========== COMPANY SP.Z.O.O ===========\n" +
                                 "----------------------------------------\n" +
                                 "Total EUR                           0.00";
        verify(display, times(1)).info(ArgumentMatchers.eq(expectedMessage));
        verify(printer, times(1)).print(ArgumentMatchers.eq(expectedReceipt));
        verifyNoMoreInteractions(display, printer);
    }

    @Test
    public void whenSomeProductsScannedAndExitThenReceiptIsPrintedAndTotalIsDisplayed() {
        provideProduct("1111111111", "Łaciate Milk 1L", "135");
        provideProduct("2222222222", "Snickers", "199");
        provideProduct("3333333333", "Pepsi 0.5L", "235");

        barcodeScanned("1111111111");
        salePoint.sale();

        barcodeScanned("2222222222");
        salePoint.sale();

        barcodeScanned("3333333333");
        salePoint.sale();

        salePoint.exit();

        String expectedMessage1 = "Łaciate Milk 1L 1.35";
        String expectedMessage2 = "Snickers 1.99";
        String expectedMessage3 = "Pepsi 0.5L 2.35";
        String expectedMessage4 = "Total 5.69";
        String expectedReceipt = "=========== COMPANY SP.Z.O.O ===========\n" +
                                 "Łaciate Milk 1L                     1.35\n" +
                                 "Snickers                            1.99\n" +
                                 "Pepsi 0.5L                          2.35\n" +
                                 "----------------------------------------\n" +
                                 "Total EUR                           5.69";

        InOrder orderVerifier = inOrder(display);
        orderVerifier.verify(display, times(1)).info(ArgumentMatchers.eq(expectedMessage1));
        orderVerifier.verify(display, times(1)).info(ArgumentMatchers.eq(expectedMessage2));
        orderVerifier.verify(display, times(1)).info(ArgumentMatchers.eq(expectedMessage3));
        orderVerifier.verify(display, times(1)).info(ArgumentMatchers.eq(expectedMessage4));
        verify(printer, times(1)).print(ArgumentMatchers.eq(expectedReceipt));
        verifyNoMoreInteractions(display, printer);
    }

    @Test
    public void invalidBarcodeNotAffectPreviouslyScannedProductsWhenExit() {
        provideProduct("1111111111", "Łaciate Milk 1L", "135");

        barcodeScanned("1111111111");
        salePoint.sale();

        barcodeScanned("");
        salePoint.sale();

        barcodeScanned("2222222222");
        salePoint.sale();

        salePoint.exit();

        String expectedMessage1 = "Łaciate Milk 1L 1.35";
        String expectedMessage2 = "Invalid bar-code";
        String expectedMessage3 = "Product not found";
        String expectedMessage4 = "Total 1.35";
        String expectedReceipt = "=========== COMPANY SP.Z.O.O ===========\n" +
                                 "Łaciate Milk 1L                     1.35\n" +
                                 "----------------------------------------\n" +
                                 "Total EUR                           1.35";

        InOrder orderVerifier = inOrder(display);
        orderVerifier.verify(display, times(1)).info(ArgumentMatchers.eq(expectedMessage1));
        orderVerifier.verify(display, times(1)).error(ArgumentMatchers.eq(expectedMessage2));
        orderVerifier.verify(display, times(1)).error(ArgumentMatchers.eq(expectedMessage3));
        orderVerifier.verify(display, times(1)).info(ArgumentMatchers.eq(expectedMessage4));
        verify(printer, times(1)).print(ArgumentMatchers.eq(expectedReceipt));
        verifyNoMoreInteractions(display, printer);
    }

    private void barcodeScanned(String barcode) {
        when(barcodeScanner.getBarcode()).thenReturn(barcode);
    }

    private void provideProduct(String barcode, String name, String priceInEurocent) {
        Product product = new Product(barcode, name, new BigDecimal(priceInEurocent));
        when(productDAO.getProductByBarcode(barcode)).thenReturn(Optional.of(product));
    }
}
